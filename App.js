import React, {Component} from 'react';
import {StyleSheet, TextInput, View, Picker, Button, Text, Image} from 'react-native';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = { num1: "", num2: "", operacao:"", estadobotao: "", resultado:"", erro:"" };
  }

  calcular = async () => {
    if (this.state.num2 === "0" && this.state.operacao === "/") {
      this.setState({resultado : ""})
      this.setState({erro: "O valor deve ser diferente de zero!"})
    }
    else {
      let total = 0;
      let num1float = parseFloat(this.state.num1);
      let num2float = parseFloat(this.state.num2);

      switch (this.state.operacao) {
        case "+": {
          total = num1float + num2float;
          this.setState({resultado:total});
          break;
        }
        case "-": {
          total = num1float - num2float;
          this.setState({resultado:total});
          break;
        }
        case "*": {
          total = num1float * num2float;
          this.setState({resultado:total});
          break;
        }
        case "/": {
          total = num1float / num2float;
          this.setState({resultado:total});
          break;
        }
    }
    this.textInput.clear();
    this.textInput.focus();
    this.textInput2.clear();
    this.setState({num1 : ""});
    this.setState({num2:""});
    }
  }

  render() {
    let botao = <Button disabled={true} title="="/> ;
    let erro;
    let resultado;
    
    if (this.state.erro === "O valor deve ser diferente de zero!" && this.state.resultado === "") {
      resultado = <Text></Text>
      erro = <Text style={{marginTop: 30, color: "#721c24", backgroundColor: "#f8d7da",
      borderColor: "#f5c6cb",  fontSize:25, padding:5}}>  {this.state.erro} </Text>;
    }
    else {
      erro = <Text></Text>
    }
      
    if (this.state.num1 !== null && this.state.num2 !== null  && this.state.num1 !== "" && this.state.num2 !== "") {
      botao = <Button color="orange" style= {{backgroundColor:"orange", }} disabled={false} title="=" onPress={this.calcular}/>
    }

    if (this.state.resultado !== null && this.state.resultado !== "") {
      resultado = <Text style={{color: "#155724", backgroundColor: "#d4edda", borderColor: "#c3e6cb", borderWidth:1, fontSize:30}}> Resultado: {this.state.resultado} </Text>
    }
    else {resultado = <Text></Text>}
    return (
      <View>
        <View style={styles.ToolbarAndroid}> 
          <Image style={styles.icon} source={require("./Calculator_5122x.png")}/>
        <Text style={styles.Titulo}> InteliCalculadora </Text>
        </View>
        <View style={styles.containerall}>
            <View style={styles.container}>
              <TextInput style={styles.TextInput}  ref={input => { this.textInput = input }} autoFocus={true} keyboardType="numeric" name="num1"  onChangeText={num1 => this.setState({ num1 })} maxLength={10}/>

              
                
              <Picker style={styles.select} selectedValue={this.state.operacao}
                            onValueChange={(operacao) => this.setState({ operacao })} mode='dialog'> 
                <Picker.Item label="operador"/>
                <Picker.Item label="+" value="+"  />
                <Picker.Item label="-" value="-"  />
                <Picker.Item label="*" value="*"  />
                <Picker.Item label="/" value="/"  />
              </Picker>
              <TextInput style={styles.TextInput}  ref={input2 => { this.textInput2 = input2 }} keyboardType="numeric" name="num2"  onChangeText={num2 => this.setState({ num2 })} maxLength={10}/>
            </View>

            <View>
              
              {botao}
                {erro}
              
              {resultado}
            
            </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ToolbarAndroid: {
    height:60,
    borderColor:"#202020",
    borderBottomWidth:1,
    display:"flex",
    alignItems:"flex-start",
    justifyContent:"center",
    flexDirection:"row"
  },
  Titulo: {
    fontSize:40,
    color:"orange",
  },
  icon : {
    marginLeft:0,
    width:60,
    height:60,
  },
  containerall: {
    height:"100%",
    backgroundColor: "#ffffe6",
    paddingLeft:20,
    paddingRight:20
  },
  container: {
    paddingTop:100,
    justifyContent: "space-between",
    alignItems: 'flex-start',
    flexDirection:"row",
    marginBottom: 20
  },
  TextInput: {
    width: 100, 
    fontSize: 30, 
    backgroundColor:"white",
    borderColor:"#101010",
    borderWidth:1,
    padding:4,
    justifyContent:"space-between"
  },
  select : {
    width: 100, 
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default App;
